#!/bin/bash

# Usage
# $ chmod +x ./wait_for_http_200.sh
# $ sh wait_for_http_200.sh http://<host>:<port>/<path>

echo "Trying to reach ${1}"
i=0
while [[ "$(curl -s -o /dev/null -w '%{http_code}' ${1})" != "200" ]]; do
	sleep 5
	let "i+=5"
	echo "Trying again after ${i} seconds. Press Ctrl + C to cancel."
done
echo 'Service OK'
