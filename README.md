## Damn Vulnerable NodeJS Application (DVNA) 

Damn Vulnerable NodeJS Application (DVNA) is a simple NodeJS application to demonstrate [**OWASP Top 10 Vulnerabilities**](https://www.owasp.org/index.php/Top_10-2017_Top_10) and guide on fixing and avoiding these vulnerabilities. The [fixes](https://github.com/appsecco/dvna/tree/fixes) branch will contain fixes for the vulnerabilities. Fixes for vunerabilities OWASP Top 10 2017 vulnerabilities at [fixes-2017](https://github.com/appsecco/dvna/tree/fixes-2017) branch.

The application is powered by commonly used libraries such as [express](https://www.npmjs.com/package/express), [passport](https://www.npmjs.com/package/passport), [sequelize](https://www.npmjs.com/package/sequelize), etc.

### Developer Security Guide book

The application comes with a **developer friendly comprehensive guidebook** which can be used to learn, avoid and fix the vulnerabilities. The guide available at https://appsecco.com/books/dvna-developers-security-guide/ covers the following

1. Instructions for setting up DVNA
2. Instructions on exploiting the vulnerabilities
3. Vulnerable code snippets and instructions on fixing vulnerabilities
4. Recommendations for avoid such vulnerabilities
5. References for learning more

The blog post for this release is at - https://blog.appsecco.com/damn-vulnerable-nodejs-application-dvna-by-appsecco-7d782d36dc1e

### Security Scans Available
- [x] Code Scan using [njsscan](https://pypi.org/project/njsscan/) - **SAST**
- [x] Project dependency check using [gitlab OWASP Dependency Check(latest CVE db)](https://gitlab.com/gitlab-ci-utils/docker-dependency-check)
- [x] **Container Security Scan** using [anchore-engine](https://docs.anchore.com/current/docs/engine/usage/integration/ci_cd/inline_scanning/)
- [x] Dynamic App Sec Test using [gitlab dast ZAP ](https://gitlab.com/gitlab-org/security-products/dast) -**DAST**
- [ ] Conditionalize DAST stage failure upon high & low priority alerts -ge zero in report.
- [ ] Link commits to fixes in documentation
### Live Test K8 deployment at https://cloud.okteto.com/

### Getting Started

DVNA can be deployed in three ways

1. For Developers, using docker-compose with auto-reload on code updates
2. For Security Testers, using the Official image from Docker Hub

Detailed instructions on setup and requirements are given in the Guide Gitbook

### Development Local Setup

Clone official repository
```bash
git clone https://github.com/appsecco/dvna; cd dvna
```

Create a `vars.env` with the desired database configuration
```
MYSQL_USER=dvna
MYSQL_DATABASE=dvna
MYSQL_PASSWORD=passw0rd
MYSQL_RANDOM_ROOT_PASSWORD=yes
```

Start the application and database using docker-compose
```bash
docker-compose up
```

Access the application at http://127.0.0.1:9090/ 

The application will automatically reload on code changes, so feel free to patch and play around with the application.

### Using Official Docker Image

Create a file named `vars.env` with the following configuration
```
MYSQL_USER=dvna
MYSQL_DATABASE=dvna
MYSQL_PASSWORD=passw0rd
MYSQL_RANDOM_ROOT_PASSWORD=yes
MYSQL_HOST=mysql-db
MYSQL_PORT=3306
```

Start a MySQL container
```bash
docker run --name dvna-mysql --env-file vars.env -d mysql:5.7
```

Start the application using the official image
```bash
docker run --name dvna-app --env-file vars.env --link dvna-mysql:mysql-db -p 9090:9090 appsecco/dvna
```

Access the application at http://127.0.0.1:9090/ and start testing! 

Note: During build stages with DIND use http://docker:9090

Flow Diagram of the pipeline: https://viewer.diagrams.net/?highlight=0000ff&edit=_blank&layers=1&nav=1#G1q-PGhGjpE5vFlNt7m0PQVFFtileowI3n