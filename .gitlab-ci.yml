#------------------------------------------------------------------------------
image: docker:latest

variables:
  IMAGE_NAME: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHA
  CONTAINER_RELEASE_IMAGE: $CI_REGISTRY_IMAGE:latest
  DOCKER_DRIVER: overlay2
  DOCKER_TLS_CERTDIR: "/certs"

services:
  - docker:dind

before_script:
#  - apk --no-cache add docker-compose
#  - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
#-------------------------------------------------------------------------------
stages:
#  - init
  - test
  - build
  - scan
  - dast
  - publish
  - deploy

# code-pull:
#   stage: init
#   allow_failure: true
#   variables:
#     GIT_STRATEGY: none
#   image: 
#     name: alpine/git
#     entrypoint: [""]
#   script:
#     - git clone https://gitlab.com/ajinabraham/dvna.git code
#     - ls code
#   cache:
#     key: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG"
#     paths:
#       - code/

sast:
  stage: test
  allow_failure: true
  image: 
    name: python
  before_script:
    - pip3 install --upgrade njsscan
  script:
    - njsscan .

dependency:
  allow_failure: true
  image:
    name: registry.gitlab.com/gitlab-ci-utils/docker-dependency-check:latest
    entrypoint: [""]
  stage: test
  script:
    # Job will scan the project root folder and fail if any vulnerabilities with CVSS > 0 are found
    - /usr/share/dependency-check/bin/dependency-check.sh --scan "./" --format ALL --project "$CI_PROJECT_NAME" --failOnCVSS 0
    # Dependency Check will only fail the job based on CVSS scores, and in some cases vulnerabilities do not
    # have CVSS scores (e.g. those from NPM audit), so they don't cause failure.  To fail for any vulnerabilities
    # grep the resulting report for any "vulnerabilities" sections and exit if any are found (count > 0).
    - if [ $(grep -c "vulnerabilities" dependency-check-report.json) -gt 0 ]; then exit 2; fi

  artifacts:
    expire_in: 4 weeks
    when: always
    paths:
        # Save the HTML and JSON report artifacts
      - "./dependency-check-report.html"
      - "./dependency-check-report.json"

container-build:
  stage: build
  script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - docker build -t $IMAGE_NAME .
    - docker push $IMAGE_NAME

container-scan:
  stage: scan
  allow_failure: true
  variables:
    ANCHORE_CLI_URL: "http://anchore-engine:8228/v1"
    GIT_STRATEGY: none
  image: docker.io/anchore/inline-scan:latest
  services:
  - name: docker.io/anchore/inline-scan:latest
    alias: anchore-engine
    command: ["start"]

  script:
  - anchore-cli system wait
  - anchore-cli registry add $CI_REGISTRY "$CI_REGISTRY_USER" "$CI_REGISTRY_PASSWORD"
  - anchore_ci_tools.py -a -r --timeout 500 --image $IMAGE_NAME
  - |
      echo "Parsing anchore vulnerability report."
      bash <<'EOF'
      for f in anchore-reports/*; do
        if [[ "$f" =~ "vuln" ]]; then
          printf "\n%s\n" "The following vulnerabilities were found on ${IMAGE_NAME}:"
          jq '[.vulnerabilities | group_by(.package) | .[] | {package: .[0].package, vuln: [.[].vuln]}]' $f || true
          exit 1
        else
          echo "No vulnerability found."
        fi
      done
      EOF

  artifacts:
    expire_in: 4 weeks
    when: always
    paths:
    - anchore-reports/*

app-sec-scan:
  stage: dast
  before_script: 
    - apk --no-cache add docker-compose
    - apk --no-cache add curl
    - apk --no-cache add bash
    - apk --no-cache add grep
  script:
    - echo "+++++Initialize Service+++++" && docker-compose up --build -d
    - docker-compose ps
    - timeout 100 /bin/bash wait_for_http_200.sh http://docker:9090/login
    - mkdir -m 777 report && cd report
    - |
        docker run --rm \
        --volume $(pwd):/zap/wrk \
        --network=scan \
        registry.gitlab.com/gitlab-org/security-products/dast:${VERSION:-latest} /analyze -t http://app:9090/login -r dast-report.html
#        --auth-url http://docker:9090/login  \
#        --auth-username 'someone' \
#        --auth-password 'p@ssw0rd' \
#        --auth-username-field 'user[login]' \
#        --auth-password-field 'user[password]' 
    - echo "+++++++Summary of Alerts++++++++"
    - grep -E "(^\s*<td><a href=\")" dast-report.html | grep -o -E 'High|Medium|Low|Informational|[0-9]+'
    - echo "++++++++++++++++++++++++++++++++"
  only:
    - tags
  artifacts:
    expire_in: 4 weeks
    when: always
    paths:
    - report/*

container-publish:
  #Publish container image once container scan report is available
  needs: [container-scan]
  stage: publish
  variables:
    GIT_STRATEGY: none
  script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
    - docker pull $IMAGE_NAME
    - if [[ $CI_COMMIT_TAG ]]; then Tag_Val=$CI_COMMIT_TAG; else Tag_Val="latest"; fi
    - docker tag $IMAGE_NAME ${CI_REGISTRY_IMAGE}:$Tag_Val
    - docker push ${CI_REGISTRY_IMAGE}:$Tag_Val

non-production:
  # Update deployment once container image is pushed in the registry
  needs: [container-publish]
  stage: deploy
  script:
    - echo "Rollout- Non Prod Environment"

production:
  stage: deploy
  # Run this stage if all the security scans stages are passed without any alarms
  when: manual
  script:
    - echo "Rollout- $CI_COMMIT_TAG"
  # Run this stage only when a tag is passed or branch is 'master'
  only:
    refs: 
      - master
    variables:
      - $CI_COMMIT_TAG != ""
     